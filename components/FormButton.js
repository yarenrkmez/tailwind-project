import React from "react";

function FormButton({ buttonName }) {
  return (
    <button className="px-4 py-3  mb-3  w-1/2 rounded-md place-self-center bg-blue-900 max-w-lg">
      {buttonName}
    </button>
  );
}

export default FormButton;
