import React from 'react'

function Navbar() {
    return (
        <div className=" bg-gray-200  m-5">
        <div className=" bg-gray-200 flex justify-around items-center m-5 ">
          <a href="#home" className="height">
            Home
          </a>
          <a href="#news">News</a>
          <a href="#contact">Contact</a>
          <a href="#about">About</a>
        </div>
      </div>
    )
}

export default Navbar
