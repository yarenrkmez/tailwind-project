import React from "react";

const FormInput = ({ type="text", placeholder }) => {
  return (
    
    <input
      type={type}
      placeholder={placeholder}
      className="form-input px-4 py-3 mb-3 w-2/3 rounded-[20px] h-[44px] place-self-center max-w-lg border-solid border border-[#A79B9B] box-border"
    />
  );
};



export default FormInput;
