module.exports = {
  mode: "jit",
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontSize: {
      loginLabelHeader:['24px','40px'],
      loginWhyJoinUs:['16px','24px'],
      loginRightSize:['22px','30px'],
      sm: ['14px', '20px'],
      base: ['16px', '24px'],
      lg: ['20px', '28px'],
      xl: ['24px', '32px'],
    },
    extend: {
      height: {
        sm: "8px",
        md: "16px",
        lg: "24px",
        xl: "48px",
      },
    },

    container: {
      center: true,
    },
    screens: {
      minsm:"280px",
      sm: "640px",

      md: "768px",

      lg: "1024px",

      xl: "1280px",
      '2xl': "1536px",
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require("@tailwindcss/forms"),
    require("@tailwindcss/forms")({
      strategy: "class",
    }),
  ],
};
