import { userActions } from "../actionTypes";

const initialState = {
  userCookie: null,
  cookieLoading: true,
};

const storiesReducer = (state = initialState, action) => {
  switch (action.type) {
    case userActions.SET_AUTH:
      return {
        ...state,
        userCookie: action.data,
        cookieLoading: false,
      };

    default:
      return state;
  }
};

export default storiesReducer;
