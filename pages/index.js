import Head from "next/head";
import { useRouter } from "next/router";
import Navbar from "../components/Navbar";
import Logo from "../public/logo.png";
import Auth from "./auth/index";

export default function Home() {
  const router = useRouter();

  return (
    <div className="  h-screen overflow-hidden flex-col">
      {/* <Navbar /> */}
      {/* <div className="  place-self-center  grid w-screen min-h-full place-items-center ">
        <div className=" flex container justify-around flex-col items-center  ">
          <p className="text-8xl text-center w-max ">MateHand</p>

          <div className="flex m-10 ">
            <button className="bg-green-400 p-6 rounded-md w-40 mr-6 transform hover:scale-110 motion-reduce:transform-none">
              Login
            </button>
            <button
              className="bg-green-400 p-6 rounded-md w-40  transform hover:scale-110 motion-reduce:transform-none"
              onClick={() => router.push("/login/Index")}
            >
              Register
            </button>
          </div>
        </div>
      </div> */}
      {/* <div class="max-w-md mx-auto  md:max-w-2xl  m-10  ">
        <div class="md:flex md:flex-row ">
          <div class="p-12">
            <div class="uppercase tracking-wide text-[5.5vw] text-indigo-500 font-semibold">
              MateHand
            </div>
            <p class="mt-2 text-gray-500">
              Getting a new business off the ground is a lot of hard work. Here
              are five ideas you can use to find your first customers.
            </p>

            <button className="bg-green-400 md:w-32 lg:w-48 rounded-md object-cover w-40 mr-6 transform hover:scale-110 motion-reduce:transform-none h-10">
              Login
            </button>

            <button
              className="bg-green-400  rounded-md w-40  transform hover:scale-110 motion-reduce:transform-none   h-10"
              onClick={() => router.push("/login/Index")}
            >
              Register
            </button>
          </div>
        </div>
      </div> */}
<Auth />
    </div>
  );
}
