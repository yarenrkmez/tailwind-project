import React from "react";
import FormInput from "../../components/FormInput";
import Polygons from "../../components/Polygons";
import WhiteIcon from "../../components/WhiteIcon";

const index = () => {
  return (
    <div className="w-screen flex minsm:flex-col lg:flex-row h-screen"  >
      <Polygons />
      <div className="flex flex-col place-self-center   ">
        <img
          src="https://play-lh.googleusercontent.com/_8wnwtaW_M7Zqc7ZrDv5GK0oY_UeKuJKAOi3z1sIHhWkbDFQYd4ufiQc3-X_5bPoXxo=s180-rw"
          className="w-14  ml-[90px]"
        ></img>

        <label className="text-center font-bold text-loginLabelHeader w-2/3 place-self-center">
          Find your illness mate and share your experiences.
        </label>

        <label className="h-6 font-[Inter] font-bold text-sm ml-[90px]">
          UserName
        </label>
        <FormInput />
        <label className="h-6 font-[Inter] font-bold text-sm  ml-[90px]">
          Password
        </label>
        <FormInput />
        <div className="flex-row ml-[90px]">
          <input
            type="checkbox"
            class="appearance-none checked:bg-blue-600 checked:border-transparent"
          ></input>
          <label className="ml-1">Remember me</label>
        </div>
        <div className=" mt-4 flex justify-between">
          <button className="ml-[90px] bg-[#00AFF1] rounded-3xl p-1 text-white  w-[90px] h-[34px] text-sm ">
            Log in
          </button>
          <button className="mr-[90px]">Forgot Password?</button>
        </div>
        <div className=" ml-[90px] mt-10 w-[65%] h-[0.5px] bg-[#A79B9B]"></div>
        <div className="flex justify-around ml-[50px] mt-[29px]">
          <label className="mr-1">Need an account?<label className="ml-1 text-[#00AFF1]">Create an Account</label> </label>
          
        </div>
      </div>
      <div className=" bg-[#00AFF1]   place-items-center flex flex-col  w-full ">
        <WhiteIcon />
        <label className=" mt-8 font-bold font-sans text-white text-loginRightSize text-center  w-[60%] ">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Porttitor
          duis sollicitudin at nulla.
        </label>
        <button className="mt-4 bg-[#00EAC8] rounded-[20px] w-[13.438rem] h-[2.75rem] p-1 text-white  text-loginWhyJoinUs font-bold">
          Why Join Us ?
        </button>
      </div>
    </div>
  );
};

export default index;
