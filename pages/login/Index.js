import React from "react";
import FormButton from "../../components/FormButton";
import FormInput from "../../components/FormInput";
import Navbar from "../../components/Navbar";

function Index() {
  return (
    <div className=" flex bg-gray-100 h-screen overflow-hidden flex-col" >
    <Navbar />
    <div className="flex container flex-col  m-3 place-self-center place-items-center ">
      <FormInput placeholder="Name" />
      <FormInput placeholder="Surname" />
      <FormInput placeholder="Username" />
      <FormInput placeholder="Email" />
      <FormInput type="password" placeholder="Password" />

      <FormButton buttonName="Kayıt Ol" />
    </div>
    </div>
  );
}

export default Index;
